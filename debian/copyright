Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: seclists
Source: https://github.com/danielmiessler/SecLists

Files: *
Copyright: Daniel Miessler <daniel@danielmiessler.com>
           Jason Haddix <jhaddix@securityaegis.com>
License: Creative Commons Attribution ShareAlike 3.0
 You are free:
 .
     to Share — to copy, distribute and transmit the work
     to Remix — to adapt the work
     to make commercial use of the work
 .
 Under the following conditions:
 .
     Attribution — You must attribute the work in the manner specified by the
     author or licensor (but not in any way that suggests that they endorse you
     or your use of the work).
 .
 With the understanding that:
 .
     Waiver — Any of the above conditions can be waived if you get permission
     from the copyright holder.
     Public Domain — Where the work or any of its elements is in the public
     domain under applicable law, that status is in no way affected by the
     license.
     Other Rights — In no way are any of the following rights affected by the
     license:
         Your fair dealing or fair use rights, or other applicable copyright
         exceptions and limitations;
         The author's moral rights;
         Rights other persons may have either in the work itself or in how the
         work is used, such as publicity or privacy rights.
     Notice — For any reuse or distribution, you must make clear to others the
     license terms of this work.
 <Put the license of the package here indented by 1 space>

Files: debian/*
Copyright: 2013 Devon Kearns <dookie@kali.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
